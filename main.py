#!python3

from tools import some_class

def do_this():
    print("Doing `this`")

def do_that():
    print("Doing `that`")

if __name__ == "__main__":
    do_this()
    do_that()

    # implement
    obj = some_class()
    flag = obj.is_ok()
    if flag:
        output_status(flag)
    print("exit")
